package JUnit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Ens permet poder executar totes les proves a la vegada.
 */

@RunWith(Suite.class)
@SuiteClasses({ inicialitzarTaulerFitxeTest.class, InicialtzarTaulerTest.class, iterarTest.class,
		taulerEvolucionaTest.class })
public class AllTestsJocDeLaVida {

}
