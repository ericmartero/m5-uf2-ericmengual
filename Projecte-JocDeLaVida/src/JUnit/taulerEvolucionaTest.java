package JUnit;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import JocDeLaVida.Iterar;

/**
 * Ens permet fer un test conforme els taulers es copien correctament.
 */

@RunWith(Parameterized.class)
public class taulerEvolucionaTest {
	
	/**
	 * Tauler del joc.
	 */
	
	private char [][] tauler;
	
	/**
	 * Tauler del test.
	 */
	
	private char [][] tauler_test;
	
	/**
     * Metode taulerEvolucionaTest, s'assignen els objectes als diferents parametres.
     * @param t1 Tauler del joc
     * @param t2 Tauler del test
     */
	
	public taulerEvolucionaTest (char [][] t1, char [][] t2) {
		
		this.tauler = t1;
		this.tauler_test = t2;
	}
	
	/**
	 * Taulers dels tests per a poder fer les comprovacions.
	 * @return Comprovacions dels taulers
	 */
	
	@Parameters
	public static Collection<Object[]> taulers() {
		
		char [][] Ta4x4 = {
				
				{'-','-','-','*'},
				{'*','*','-','-'},
				{'-','-','-','-'},
				{'-','*','-','*'},
		};
		
		char [][] Ta5x4 = {
				
				{'-','-','-','-'},
				{'-','*','-','-'},
				{'-','-','*','-'},
				{'-','*','-','-'},
				{'-','-','-','*'},
		};
		
		char [][] Ta4x8 = {
				
				{'-','-','-','-','*','-','-','-'},
				{'-','*','-','-','-','-','-','*'},
				{'-','-','-','-','*','-','*','-'},
				{'-','-','*','-','-','-','-','*'},
		};
		
		char [][] Ta6x4 = {
				
				{'-','-','*','-'},
				{'*','-','-','-'},
				{'-','-','*','-'},
				{'*','-','-','-'},
				{'-','*','-','*'},
				{'-','-','*','-'},
		};
		
		char [][] Ta7x8 = {
				
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
		};
		
		char [][] Ta5x10 = {
				
				{'-','-','*','-','-','*','-','-','-','-'},
				{'-','-','-','-','-','-','-','*','-','-'},
				{'-','*','-','-','*','-','-','-','-','*'},
				{'-','-','*','-','-','-','*','-','-','-'},
				{'-','*','-','*','-','-','-','*','-','-'},
		};
		
		char [][] Ta9x9 = {
				
				{'-','-','-','-','*','-','-','-','-'},
				{'-','-','-','-','*','-','-','-','-'},
				{'-','-','-','-','*','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'*','*','*','-','-','-','*','*','*'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','*','-','-','-','-'},
				{'-','-','-','-','*','-','-','-','-'},
				{'-','-','-','-','*','-','-','-','-'},
		};
		
		char [][] Ta7x7 = {
				
				{'-','-','-','-','-','-','-'},
				{'-','-','*','-','*','-','-'},
				{'-','*','-','*','-','*','-'},
				{'-','-','-','-','-','-','-'},
				{'-','-','*','-','*','-','-'},
				{'-','*','-','-','-','*','-'},
				{'*','-','-','-','-','-','*'},
		};
		
		char [][] Ta7x9 = {
				
				{'-','-','-','*','-','-','-','-','-'},
				{'-','*','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','*','-','-'},
				{'-','-','-','*','*','-','-','-','-'},
				{'-','*','-','-','-','-','-','*','-'},
				{'-','-','-','-','-','*','-','-','-'},
				{'-','-','*','-','-','-','-','*','-'},
		};
		
		char [][] Ta8x4 = {
				
				{'-','-','-','-'},
				{'-','*','-','-'},
				{'-','-','-','*'},
				{'-','-','*','-'},
				{'-','*','-','-'},
				{'*','-','-','-'},
				{'-','-','*','-'},
				{'-','*','-','-'},
		};
		
		return Arrays.asList(new Object[][] {
			{Ta4x4, Ta4x4},
			{Ta5x4, Ta5x4},
			{Ta4x8, Ta4x8},
			{Ta6x4, Ta6x4},
			{Ta7x8, Ta7x8},
			{Ta5x10, Ta5x10},
			{Ta9x9, Ta9x9},
			{Ta7x7, Ta7x7},
			{Ta7x9, Ta7x9},
			{Ta8x4, Ta8x4},
		});
		
	}
	
	/**
     * Metode taulerEvoluciona, compara el tauler del test amb el tauler del joc.
     */
	
	@Test		
	public void taulerEvoluciona() {
		
		char [][] tauler_joc  = Iterar.evoluciona(tauler);
		assertArrayEquals(tauler_test,  tauler_joc);
	}
}
