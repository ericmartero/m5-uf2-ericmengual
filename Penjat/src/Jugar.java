import java.util.Random;
import java.util.Scanner;

/**
 * <h2>Classe Jugar, que tindr� els m�todes necessaris per poder jugar una partida </h2>
 * 
 * Cerca informaci� de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 2-2022
 * @author emengual
 * @since 14-2-2022
 */

/*
 * JOC DEL PENJAT
 */

public class Jugar {
	
	/**
     * Demanem un valor per teclat
     */
	
	static Scanner src = new Scanner(System.in);
	
	/**
     * Permetr� jugar una partida si el jugador ja est� definit. 
     * @see Jugador
     * @param jugador Valor del jugador que juga la partida
     */
	
	public static void jugarPartida(String jugador) {
		// TODO Auto-generated method stub
		
		//Estructura de dades del joc

		String [] llistaParaules;           //Llista de paraules per jugar al penjat

		String paraula;   					//Guarda la paraula a esbrinar
		char [] taulell;						//paraula en proc�s de joc
		char [] lletres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();   
											//vector amb les lletres disponibles durant el joc
		int fallos;						//comptador d'intents
		boolean guanya;						//indica si el jugador ha guanyat o ha perdut
		boolean haFallat;
		char lletra;							//guarda la letra seleccionada por el usuario en cada tirada
		
		final int TOTAL_FALLOS = 14; 	//constante que contiene el m�ximo de fallos permitidos para poder ganar la partida
		
		//Inicialitzaci� de les variables del joc
		benvinguda(jugador);
		llistaParaules = inicialitzaLlista();
		paraula = escullParaula(llistaParaules);
		taulell = inicialitzaTaulell(paraula.length());
		fallos = 0;
		guanya = false;
		
		//Comencem a jugar
		while (fallos <= TOTAL_FALLOS && !guanya) {
			mostrar(lletres, taulell, fallos);
			lletra = llegirLletra(lletres);
			haFallat = actualitzarTaulell(lletra, paraula, taulell);
			if (haFallat) fallos++;
			guanya = comprovarTaulell(taulell, paraula);
		}
		mostrarResultats(taulell, fallos, lletres, paraula, guanya);	
		
		Jugador.actualitzarJugador(guanya, jugador);
		
	}

	/**
     * Permetr� donar la benvinguda si el jugador ja esta definit. 
     * @see Jugador
     * @param jugador Valor del jugador al qui es dona la benvinguda
     */

	private static void benvinguda(String jugador) {
		// TODO Auto-generated method stub
		System.out.println("Benvingut " + jugador + "!!\n\nComen�a la partida del penjat");
	}

	/**
     * Permetr� mostrar els resultats de la partida jugada.
     * @param taulell Mostra el taulell de la partida
     * @param fallos Valor num�ric dels erros fets durant la partida
     * @param lletres Mostra les lletres que ha introduit el jugador per a esbrinar la paraula
     * @param paraula Mostra la paraula a esbrinar
     * @param guanya Ens monstra si el jugador ha guanyat la partida o l'ha perduda
     */

	private static void mostrarResultats(char[] taulell, int fallos, char[] lletres, String paraula, boolean guanya) {
		// Mostra per pantalla el resultat de la partida
		mostrar(lletres, taulell, fallos);
		if (guanya)
			System.out.println("Felicitats!!!! \n\nHas guanyat la partida");
		else {
			System.out.println("Ops!!!! \n\nHas perdut la partida");
			System.out.println("La paraula que havies d'esbrinar �s: " + paraula);
		}
	}

	/**
     * Permetr� comprovar si la paraula esta al taulell.
     * @param taulell Paraula en proc�s de joc
     * @param paraula Mostra la paraula a esbrinar
     * @return <ul>
     *           <li>true: la paraula esta al taulell</li>
     *           <li>false: la paraula no esta al taulell</li>
     *         </ul>
     */

	private static boolean comprovarTaulell(char[] taulell, String paraula) {
		// Compara el taulell amb la paraula. Si s�n iguals retorna true, sino, retorna false
		
		int i = 0;
		while (i < paraula.length() && paraula.charAt(i) == taulell[i]) i++;
		if (i < paraula.length()) return false;
		
		return true;
	}

	/**
     * Permetr� comprovar si la lletra es troba o no dins de la paraula i la copia al taulell tantes vegades com la trobi.
     * @param lletra Desa la lletra seleccionada per l'usuari en cada tirada
     * @param paraula Mostra la paraula a esbrinar
     * @param taulell Paraula en proc�s de joc
     * @return <ul>
     *           <li>true: la lletra no es troba dins la paraula</li>
     *           <li>false: la lletra es troba dins la paraula</li>
     *         </ul>
     */

	private static boolean actualitzarTaulell(char lletra, String paraula, char[] taulell) {
		// Comprova si la lletra es troba o no dins la paraula i la copia a taulell tantes vegades com la trobi
		boolean fallat = true;
		for (int i = 0; i < paraula.length(); i++) {
			if (paraula.charAt(i) == lletra)  {
				taulell[i] = lletra;
				fallat = false;
			}
		}
		
		return fallat;
	}

	/**
     * M�tode que llegeix una lletra i valida que sigui correcta i actgualitza el vector de lletres posant la lletra a '*'.
     * @param lletres Mostra les lletres que ha introduit el jugador per a esbrinar la paraula
     * @return Lletra llegida
     */

	private static char llegirLletra(char[] lletres) {
		// Llegeix una lletra i valida que sigui correcta i actgualitza el vector de lletres posant la lletra a '*' Retorna la lletra llegida
		char lletra;
		boolean correcte = false;
		
		do {
			System.out.print("Indica la lletra que vols posar: ");
			lletra = src.nextLine().toUpperCase().charAt(0);
			while (!(lletra >= 'A' && lletra <= 'Z')) {
				System.out.println("Error, lletra no v�lida");
				lletra = src.nextLine().toUpperCase().charAt(0);
			}

			int index = lletra - 'A';
			if (lletres[index] != '*') {
				lletres [index] = '*';
				correcte = true;
			}
			else System.out.println("La lletra ja est� gastada");
		} while (correcte == false);
			
		
		return lletra;
	}

	/**
     * M�tode que mostra per pantalla totes les dades del joc, que rep amb els arguments.
     * @param lletres  Mostra les lletres que ha introduit el jugador per a esbrinar la paraula
     * @param taulell Paraula en proc�s de joc
     * @param fallos Comptador d'intents
     */
	
	private static void mostrar(char[] lletres, char[] taulell, int fallos) {
		// mostra per pantalla totes les dades del joc, que rep amb els arguments
		System.out.println("Seg�ent jugada\n\nPortes " + fallos + " fallides");
		
		//mostrem les lletres disponibles
		for (int i = 0; i < lletres.length; i++) System.out.print(lletres[i] + " ");
		System.out.println();
		
		//mostrem el taulell
		for (int i = 0; i < taulell.length; i++) System.out.print(taulell[i] + " ");
		System.out.println();
		
	}

	/**
     * M�tode que rep la longitud del vector de char, crea el vector de char i ho inicialitza a '-'.
     * @param length Ens indica el valor de la longitud del vector de char
     * @return Vector creat
     */

	private static char[] inicialitzaTaulell(int length) {
		// Rep la longitud del vector de char, crea el vector de char i ho inicialitza a '-'
		char [] vector = new char[length];
		for (int i = 0; i < length; i++) vector[i] = '_';
		return vector;
	}

	/**
     * M�tode que selecciona una paraula de la llista que es rep per par�metre.
     * @param llistaParaules Llista de paraules generades
     * @return Paraula seleccionada
     */

	private static String escullParaula(String[] llistaParaules) {
		// Selecciona una paraula de la llista que es rep per par�metre i retorna la paraula seleccionada
		Random rnd = new Random();
		
		int index = rnd.nextInt(llistaParaules.length);
		
		return llistaParaules[index];
	}
	
	/**
     * M�tode que genera una llista de paraules en un vector d'Strings.
     * @return Llista de paraules
     */
	
	private static String[] inicialitzaLlista() {
		// M�tode que genera una llista de paraules en un vector d'Strings
		String [] llista = { "CASA", "LLIBRE", "RELLOTGE" };
	
		return llista;
	}

}
