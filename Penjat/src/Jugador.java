import java.util.ArrayList;
import java.util.Scanner;

/**
 * <h2>Classe Jugador, que contindr� els m�todes necessaris per afegir  i/o veure el jugador </h2>
 * 
 * Cerca informaci� de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 2-2022
 * @author emengual
 * @since 14-2-2022
 */

public class Jugador {
	
	/**
     * Demanem un valor per teclat.
     */
	
	static Scanner src = new Scanner(System.in);
	
	/**
     * Array que desa el noms dels jugadors.
     */
	
	static ArrayList<String> noms = new ArrayList<String>();  //guarda els noms dels jugadors
	
	/**
     * Array que desa el nombre de partides guanyades per a cada jugador.
     */
	
	static ArrayList<Integer> pGuanya = new ArrayList<Integer>();  //guarda el nombre de partides guanyades per cada jugador
	
	/**
     * Array que desa el nombre ed partides perdudes per a cada jugador.
     */
	
	static ArrayList<Integer> pPerduda = new ArrayList<Integer>(); //guarda el nombre de partides perdudes per cada jugador
	
	/**
     * Demanem el nom del jugador. Si el jugador no existeix, el donem d'alta
     * @return Nom del jugador
     */
	
	public static String definirJugador() {
		//Demanem el nom del jugador. Si el jugador no existeix, el donem d'alta
		String nom;
		
		System.out.print("Indica el nom del jugador");
		nom = src.nextLine().toUpperCase();
		
		if (!noms.contains(nom)) {
			noms.add(nom);
			pGuanya.add(0);
			pPerduda.add(0);
		}
		
		return nom;
	}
	
	/**
     * Mostra les dades del jugador que rep per par�metre.
     * @param jugador Valor del jugador que juga la partida
     */

	public static void mostrarJugador(String jugador) {
		// Mostra les dades del jugador que rep per par�metre
		if (noms.contains(jugador)) {
			int pos = noms.indexOf(jugador);
			System.out.println("Resultats: ");
			System.out.println("Partides guanyades:  " + pGuanya.get(pos));
			System.out.println("Partides perdudes:  " + pPerduda.get(pos));
		}
		
	}
	
	/**
     * Mostra les dades del jugador que rep per par�metre.
     * @param guanya Ens monstra si el jugador ha guanyat la partida o l'ha perduda
     * @param jugador Valor del jugador que juga la partida
     */
	
	public static void actualitzarJugador(boolean guanya, String jugador) {
		
		if (noms.contains(jugador)) {
			int pos = noms.indexOf(jugador);
		
			if (guanya == true) pGuanya.set(pos, pGuanya.get(pos) + 1);
			else pPerduda.set(pos,  pPerduda.get(pos) + 1);
		}
	}

}
