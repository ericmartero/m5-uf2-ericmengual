package JocDeLaVida;

/**
 * <h2>Classe VisualitzarTauler, permet visualitzar el tauler en qualsevol moment del joc. </h2>
 * 
 * Cerca informacio de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 3-2022
 * @author emengual
 * @since 8-3-2022
 */

public class VisualitzarTauler {
	
	/**
     * Metode visualitzarTauler, que permet visualitzar el tauler si aquest ha estat inicialitzat anteriorment amb celulues aleatories o fitxes.
     * @see InicialitzarTauler
     * @param tauler Tauler del joc
     * @return Tauler del joc
     */
	
	public static char[][] visualitzarTauler(char tauler[][]) {
		
        for (int f = 0; f < tauler.length; f++) {
            for (int c = 0; c < tauler[f].length; c++) {
            	
                System.out.print(tauler[f][c] + " ");
            }
            System.out.println();
        }
		System.out.println("\n");
		
		return tauler;
	}
}
