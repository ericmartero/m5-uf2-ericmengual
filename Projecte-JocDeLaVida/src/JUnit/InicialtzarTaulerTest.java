package JUnit;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import JocDeLaVida.InicialitzarTauler;

/**
 * Ens permet fer un test conforme el tauler del joc sigui el correcte.
 */
	
@RunWith(Parameterized.class)
public class InicialtzarTaulerTest {
	
	/**
	 * Files del tauler.
	 */
	
	private int files;
	
	/**
	 * Columnes del tauler.
	 */
	
	private int columnes;
	
	/**
	 * Tauler del test.
	 */
	
	private char [][] tauler_test;
	
	/**
     * Metode InicialtzarTaulerTest, s'assignen els objectes als diferents parametres.
     * @param f Files del tauler
     * @param c Columnes del tauler
     * @param t1 Tauler de proves
     */
	
	public InicialtzarTaulerTest (int f, int c, char [][] t1) {
		this.files = f;
		this.columnes = c;
		this.tauler_test = t1;
	}
	
	/**
	 * Taulers dels tests per a poder fer les comprovacions.
	 * @return Comprovacions dels taulers
	 */
	
	@Parameters
	public static Collection<Object[]> taulers() {
		
		char [][] Ta4x4 = {
				
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
		};
		
		char [][] Ta5x4 = {
				
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
		};
		
		char [][] Ta4x8 = {
				
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
		};
		
		char [][] Ta6x4 = {
				
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
		};
		
		char [][] Ta7x8 = {
				
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-'},
		};
		
		char [][] Ta5x10 = {
				
				{'-','-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-','-'},
		};
		
		char [][] Ta9x9 = {
				
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
		};
		
		char [][] Ta7x7 = {
				
				{'-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-'},
		};
		
		char [][] Ta7x9 = {
				
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
		};
		
		char [][] Ta8x4 = {
				
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
				{'-','-','-','-'},
		};
		
		return Arrays.asList(new Object[][] {
			{4, 4, Ta4x4},
			{5, 4, Ta5x4},
			{4, 8, Ta4x8},
			{6, 4, Ta6x4},
			{7, 8, Ta7x8},
			{5, 10, Ta5x10},
			{9, 9, Ta9x9},
			{7, 7, Ta7x7},
			{7, 9, Ta7x9},
			{8, 4, Ta8x4},
			
		});
		
	}
	
	/**
     * Metode InicialtzarTauler, compara el tauler del test amb el tauler del joc.
     */

	@Test		
	public void InicialtzarTauler() {
		
		char [][] tauler_joc  = InicialitzarTauler.inicialitzarTauler(files, columnes);
		assertArrayEquals(tauler_test,  tauler_joc);
	}

}
