package JocDeLaVida;

import java.util.Random;

/**
 * <h2>Classe InicialitzarTauler, contindra el metodes per a poder inicialitzar el tauler amb celules vives aleatoriament o fitxes. </h2>
 * 
 * Cerca informacio de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 3-2022
 * @author emengual
 * @since 8-3-2022
 */

public class InicialitzarTauler {
	
	/**
     * Ens permet poder obtenir un valor aleatori de les dades al programa.
     */
	
	static Random rnd = new Random();
	
	/**
     * Metode MORT, ens permet indicar que les caselles mortes es representen amb el caracter '-'.
     */
	
	public static char MORT = '-';
	
	/**
     * Metode VIU, ens permet indicar que les caselles mortes es representen amb el caracter '*'.
     */
	
	public static char VIU = '*';
	
	/**
     * Metode inicialitzarTauler, que inicialitza el tauler del joc amb totes les celules en estat mortes.
     * @param files Nombre de files del taulell
     * @param columnes Nombre de columnes del taulell
     * @return Tauler del joc
     */
	
	public static char[][] inicialitzarTauler(int files, int columnes) {
		
		char tauler[][] = new char[files][columnes];
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				tauler[f][c] = MORT;
			}
		}
		
		return tauler;
	}
	
	/**
     * Metode inicialitzarTaulerFitxe, que inicialitza el tauler del joc amb un nombre de celules vives fitxe.
     * @return Tauler del joc
     */
	
	public static char[][] inicialitzarTaulerFitxe() {
		
		char tauler[][] = {
				 
	            {MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT},
	            {MORT, MORT, MORT, VIU, VIU, VIU, MORT, MORT, MORT},
	            {MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT},
	            {MORT, VIU, MORT, MORT, MORT, MORT, MORT, VIU, MORT},
	            {MORT, VIU, MORT, MORT, MORT, MORT, MORT, VIU, MORT},
	            {MORT, VIU, MORT, MORT, MORT, MORT, MORT, VIU, MORT},
	            {MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT},
	            {MORT, MORT, MORT, VIU, VIU, VIU, MORT, MORT, MORT},
	            {MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT, MORT},

		};

		System.out.println("\nS'ha inicialitzat correctament el tauler\n");

		return tauler;
	}
	
	/**
     * Metode taulerDimensionat, que dimensiona el tauler del joc amb les celules vives sortegades.
     * @param f Numero de files del taulell
     * @param c Numero de columnes del taulell
     * @return Tauler del joc dimensionat
     */
	
	public static char[][] taulerDimensionat(int f, int c) {
		
		//Dimensionem el tauler amb les celules vives aleatories
		
		char tauler[][] = inicialitzarTauler(f, c);
		
		int num_vives = sorteigVives(f, c);
		tauler = colocarVives(tauler, f, c, num_vives);
		
		System.out.println("\nS'ha inicialitzat correctament el tauler\n");
		
		return tauler;
	}
	
	/**
     * Metode sorteigVives, que sortega el nombre de celules vives que tindra el tauler.
     * @param f Numero de files del taulell
     * @param c Numero de columnes del taulell
     * @return Nombre de celules vives que tindra el tauler.
     */
	
	public static int sorteigVives(int f, int c) {
		
		int caselles;
		caselles = f * c;
		int num_vives = rnd.nextInt(caselles/4) + caselles/4;
		
		return num_vives;
	}
	
	/**
     * Metode colocarVives, que sortega el nombre de celules vives que tindra el taulell.
     * @param tauler Tauler del joc
     * @param f Numero de files del tauler
     * @param c Numero de columnes del tauler
     * @param num_vives Nombre de celules vives que tindra el tauler
     * @return Tauler del joc amb les celules vives colocades.
     */
	
	public static char[][] colocarVives(char [][] tauler, int f, int c, int num_vives) {
		
		//Carrega el tauler amb les celules en estat vives.
		
		while (num_vives > 0) {
			
			int posFila = rnd.nextInt(f);
			int posColumna = rnd.nextInt(c);
			
            if (tauler[posFila][posColumna] == MORT) {
            	
            	tauler[posFila][posColumna] = VIU;
            	
            	num_vives--;
            }  
		}
		
		return tauler;
	}

}
