package JUnit;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import JocDeLaVida.Iterar;

/**
 * Ens permet fer un test conforme les diferents interacions dels taulers siguin correctes.
 */

@RunWith(Parameterized.class)
public class iterarTest {
	
	/**
	 * Tauler del joc.
	 */
	
	private char [][] tauler;
	
	/**
	 * Tauler del test.
	 */
	
	private char [][] tauler_test;
	
	/**
	 * Nombre d'iteracions.
	 */
	
	private int numIter;
	
	/**
     * Metode iterarTest, s'assignen els objectes als diferents parametres.
     * @param t Tauler del joc
     * @param tauTest Tauler del test
     * @param num Nombre d'iteracions
     */
	
	public iterarTest(char[][] t, char [][] tauTest, int num) {
		this.tauler = t;
		this.tauler_test = tauTest;
		this.numIter = num;
	}
	
	/**
	 * Taulers dels tests per a poder fer les comprovacions.
	 * @return Comprovacions dels taulers
	 */
	
	@Parameters
	public static Collection<Object[]> taulers() {
		
		char [] [] [] taulerAleatori = {
				
				{ {'*','*','-','*'},
				  {'-','-','-','-'},
				  {'-','-','*','*'},
				  {'*','-','-','-'}
				},
				{ {'-','-','-','-'},
				  {'-','-','-','-'},
				  {'-','-','-','-'},
				  {'-','-','-','-'}
				}
		};
		
		char [] [] [] taulerFitxe = {
				
				{ {'-','-','-','-','-','-','-','-','-'},
				  {'-','-','-','*','*','*','-','-','-'},
				  {'-','-','-','-','-','-','-','-','-'},
				  {'-','*','-','-','-','-','-','*','-'},
				  {'-','*','-','-','-','-','-','*','-'},
				  {'-','*','-','-','-','-','-','*','-'},
				  {'-','-','-','-','-','-','-','-','-'},
				  {'-','-','-','*','*','*','-','-','-'},
				  {'-','-','-','-','-','-','-','-','-'}
				},
				{ {'-','-','-','-','*','-','-','-','-'},
				  {'-','-','-','-','*','-','-','-','-'},
				  {'-','-','-','-','*','-','-','-','-'},
				  {'-','-','-','-','-','-','-','-','-'},
				  {'*','*','*','-','-','-','*','*','*'},
				  {'-','-','-','-','-','-','-','-','-'},
				  {'-','-','-','-','*','-','-','-','-'},
				  {'-','-','-','-','*','-','-','-','-'},
				  {'-','-','-','-','*','-','-','-','-'}
				}
		};
		
		return Arrays.asList(new Object[][] {
			
			{taulerAleatori[0], taulerAleatori[1], 4},
			{taulerFitxe[0], taulerFitxe[1], 3},
			{taulerFitxe[0], taulerFitxe[1], 1},
			{taulerFitxe[0], taulerFitxe[1], 61},
			{taulerFitxe[0], taulerFitxe[1], 23},
		});
	}
	
	/**
     * Metode Iterar, compara el tauler del test amb el tauler del joc.
     */
	
	@Test
	public void Iterar() {
		char [][] tauler_joc = Iterar.iterar(tauler, numIter);
		
		assertArrayEquals(tauler_test, tauler_joc);
	}
}
