package JUnit;

import static org.junit.Assert.assertArrayEquals;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import JocDeLaVida.InicialitzarTauler;

/**
 * Ens permet fer un test conforme el tauler fitxe del joc sigui el correcte.
 */

@RunWith(Parameterized.class)
public class inicialitzarTaulerFitxeTest {
	
	/**
	 * Tauler del test per a comparar amb el tauler del joc.
	 */
	
	private char [][] tauler_test;
	
	/**
     * Metode inicialitzarTaulerFitxeTest, assigna el objecte tauler_test al tauler de proves.
     * @param t1 Tauler de proves
     */
	
	public inicialitzarTaulerFitxeTest (char [][] t1) {
		
		this.tauler_test = t1;
	}
	
	/**
	 * Taulers dels tests per a poder fer les comprovacions.
	 * @return Comprovacions dels taulers
	 */
	
	@Parameters
	public static Collection<Object[]> tauler() {
		
		char [][] Ta9x9 = {
				
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','*','*','*','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','*','-','-','-','-','-','*','-'},
				{'-','*','-','-','-','-','-','*','-'},
				{'-','*','-','-','-','-','-','*','-'},
				{'-','-','-','-','-','-','-','-','-'},
				{'-','-','-','*','*','*','-','-','-'},
				{'-','-','-','-','-','-','-','-','-'},
		};
		
		
		return Arrays.asList(new Object[][] {
			
			{Ta9x9},
			
		});
		
	}
	
	/**
     * Metode InicialtzarTaulerFitxe, compara el tauler del test amb el tauler del joc.
     */

	@Test		
	public void InicialtzarTaulerFitxe() {
		
		char [][] tauler_joc  = InicialitzarTauler.inicialitzarTaulerFitxe();
		assertArrayEquals(tauler_test,  tauler_joc);
	}
}
