package JocDeLaVida;

/**
 * <h2>Classe Iterar, contindra el metodes per a poder iterar el tauler aplicant unes regles especifiques.</h2>
 * 
 * Cerca informacio de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 3-2022
 * @author emengual
 * @since 8-3-2022
 */

public class Iterar {
	
	/**
     * Metode iterar, que itera el tauler tantes vegades com el usuari indiqui.
     * @param tauler Tauler del joc
     * @param numIter Nombre d'iteracions
     * @return Tauler del joc
     */
	
	public static char[][] iterar(char[][] tauler, int numIter) {
		
		boolean viva;
		
		System.out.println("\nTAULER INICIAL\n");
		VisualitzarTauler.visualitzarTauler(tauler);
		
		for (int i = 1; i < numIter +1; i++) {
			
			char[][] taulerIterat = InicialitzarTauler.inicialitzarTauler(tauler.length, tauler[0].length);
			
			for (int f = 0; f < tauler.length; f++) {
				for (int c = 0; c < tauler[0].length; c++) {
					
			        //Comprovacio estat celules, apliquem les regles especificades
					
                    if (tauler[f][c] == InicialitzarTauler.MORT) {
                        viva = comprovarCaselles(tauler, f, c, false);
                    } 

                    else {
                    	viva = comprovarCaselles(tauler, f, c, true);
                    }
                    
                    if (viva) {
                    	taulerIterat[f][c] = InicialitzarTauler.VIU;
                    }
                    
                    else {
                    	taulerIterat[f][c] = InicialitzarTauler.MORT;
                    }
				}
			}
			
			tauler = evoluciona(taulerIterat);
			
			System.out.println("\nTAULER EVOLUCIONAT - ITERACIO " + i + "\n");
			VisualitzarTauler.visualitzarTauler(tauler);
		}
		
		return tauler;
	}
	
	
	/**
     * Metode evoluciona, que copia el contingut del tauler iterat al tauler del joc.
     * @param taulerIterat Tauler iterat
     * @return Tauler del joc
     */
	
	public static char[][] evoluciona(char[][] taulerIterat) {
		
		char [][] tauler = new char[taulerIterat.length][taulerIterat[0].length];
		
		for (int f = 0; f < taulerIterat.length; f++) {
			for (int c = 0; c < taulerIterat[0].length; c++) {
				tauler[f][c] = taulerIterat[f][c];
			}
		}
		
		return tauler;
	}
	
	/**
	 * Metode comprovarCaselles, que comprova les celules vives que hi han al voltant d'una i retorna un boolean segons unes condicions.
	 * @param tauler Tauler del joc
	 * @param f Nombre de files del taulell
	 * @param c Nombre de columnes del taulell
	 * @param viva Boolean que ens indica si una celula es viva o morta
	 * @return <ul>
	 * 				<li>True: Si la celula es morta i te 3 vives al voltant o si la celula es viva i te 2 o 3 vives al voltant
	 * 				<li>False: Si no es compleixen les condicions</li>
	 * 		   </ul>
	 */
	
	public static boolean comprovarCaselles(char[][] tauler, int f, int c, boolean viva) {
		
		int contVives = 0;
		
		//Comprovem les celules vives del voltant
		
		try{
		    if(tauler[f-1][c-1] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f-1][c] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f-1][c+1] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f][c-1] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f][c+1] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f+1][c-1] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f+1][c] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
		 
		try{
		    if(tauler[f+1][c+1] == InicialitzarTauler.VIU)contVives++;
		}catch(java.lang.ArrayIndexOutOfBoundsException e){}
	    
		
	     if (!viva && contVives == 3) {
	    	 return true;
	     }
	     
	     else if (viva && contVives == 2 || contVives == 3) {
	    	 return true;
	     }
	     
	     else {
	    	 return false;
	     }
	}
}
