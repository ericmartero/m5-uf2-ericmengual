
import java.util.Scanner;

/**
 * <h2>Classe Programa, contindr�el m�tode main() que far� de distribuidor de tasques, que es troben implementades a les classes descrites abans </h2>
 * 
 * Cerca informaci� de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 2-2022
 * @author emengual
 * @since 14-2-2022
 */

//Classe que fará de distribuidor de tasques, que es troben implementades a les classes descrites abans.
public class Programa {
	
	/**
     * Demanem un valor per teclat.
     */
	
	static Scanner src = new Scanner(System.in);

	/**
     * M�tode per a la distribuci� de tasques, que seran les que implementaran les diferents opcions del programa.
     * @param args Main del programa
     */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int opcio;
		String jugador = null;
		boolean definit = false;
		
		do {
			opcio = menu();
			switch (opcio) {
				case 1: Ajuda.mostrarInfo();
						break;
				case 2: jugador = Jugador.definirJugador();
						definit = true;
						break;
				case 3: if (definit) {
							Jugar.jugarPartida(jugador);
							definit = false;
						}
						else System.out.println("Error, cal definir primer l'usuari");
						break;
				case 4: Jugador.mostrarJugador(jugador);
						break;
				case 0: System.out.println("Moltes gr�cies per jugar\n\nAdeu!!!");
						break;
				default: System.out.println("Error, opci� incorrecta. Torna-ho a provar..");
			}
			
		} while (opcio != 0);
	}

	/**
     * M�tode en el qual ens mostra el men� del joc.
     * @return Opcio del men� escollida
     */
	
	private static int menu() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;
		
		System.out.println("1.- Mostrar Ajuda");
		System.out.println("2.- Definir Jugador");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.- Veure Jugador");
		System.out.println("0.- Sortir");
		System.out.println("\nEscull una opci�: ");
		
		do {
			try {
				op = src.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un n�mero enter");
				src.nextLine();
			}
		} while(!valid);
		
		return op;
	}

}
