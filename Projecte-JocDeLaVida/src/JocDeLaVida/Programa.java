package JocDeLaVida;
import java.util.Scanner;

/**
 * <h2>Classe Programa, contindra el metode main() que fara de distribuidor de tasques, que es troben implementades al menu del joc. </h2>
 * 
 * Cerca informacio de Javadoc a <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 3-2022
 * @author emengual
 * @since 8-3-2022
 */

public class Programa {
	
	/**
     * Ens permet poder entrar per teclat dades al programa.
     */
	
	static Scanner sc = new Scanner(System.in);
	
	/**
     * Metode main, per a la distribucio de tasques, que seran les que implementaran les diferents opcions del programa.
     * @param args Parametre per defecte
     */
	
	public static void main(String[] args) {
		
		int opcio;
		char[][] tauler = null;
		boolean valid;
		int f, c;
		int numIter = 0;
		
		do {
			opcio = menu();
			switch (opcio) {
				case 1: valid = false;
					do {
						try {
							
							System.out.println("\nNombre de files:");
							f = sc.nextInt();
							System.out.println("Nombre de columnes:");
							c = sc.nextInt();
							
							if ((f < 4 || f > 10) || (c < 4 || c > 10)) {
								
								System.out.println("\nERROR: Ha de ser un numero entre el 4 i el 10\n");
							}
							
							else {
								
								tauler = InicialitzarTauler.taulerDimensionat(f, c);
								valid = true;
							}
						}
						
						catch (Exception e) {
							
							System.out.println("ERROR: Has de posar un numero enter\n");
							sc.nextLine();
						}
					}while (!valid);
					
					break;
					
				case 2: 
					
					tauler = InicialitzarTauler.inicialitzarTaulerFitxe();
					
					break;
					
				case 3: 
					if (tauler == null) {
						System.out.println("ERROR: Has de inicialitzar el tauler primer!\n");
					}
					
					else {
						System.out.println("\nTAULER - JOC VIDA\n");
						VisualitzarTauler.visualitzarTauler(tauler);
					}
					
					break;
					
				case 4: valid = false;
					do {
						try {
							if (tauler == null) {
								System.out.println("ERROR: Has de inicialitzar el tauler primer!\n");
							}
						
							else {
								
								System.out.println("Nombre d'iteracions: ");
								numIter = sc.nextInt();
								
								if (numIter < 0 || numIter > 100) {
									
									System.out.println("ERROR: Ha de ser un nombre entre el 0 i el 100\n");
								}
								
								else {
									tauler = Iterar.iterar(tauler, numIter);
									valid = true;
								}
							}
						}
						
						catch (Exception e) {
							System.out.println("\nERROR: Has de posar un numero enter\n");
							sc.nextLine();
						}
						
					}while(!valid);
					
					break;
					
				case 0: System.out.println("Moltes gracies per jugar\n\nAdeu!!");
						break;
				default: System.out.println("\nERROR: Opcio incorrecta. Torna-ho a provar..\n");
			}
			
		} while (opcio != 0);
	}
	
	/**
     * Metode menu, en el qual ens mostra el menu principal del joc.
     * @return Opcio del menu escollida per l'usuari
     */
	
	public static int menu() {
		
		int entrada = 0;
		boolean enter = false;
		
		System.out.println("JOC DE LA VIDA\n");
		System.out.println("1.- Inicialitzar tauler");
		System.out.println("2.- Inicialitzar tauler fix");
		System.out.println("3.- Visualitzar tauler");
		System.out.println("4.- Iterar");
		System.out.println("0.- Sortir");
		System.out.println("\nEscull una opcio:");
		
		do {
			try {
				entrada = sc.nextInt();
				enter = true;
			}
			
			catch (Exception e) {
				System.out.println("ERROR: has de posar un nombre enter");
				sc.nextLine();
			}
			
		} while(!enter);
		
		return entrada;
	}
}
